﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введiть початкове значення Xmin: ");
            string sxMin = Console.ReadLine();
            double xMin = Double.Parse(sxMin);
            Console.Write("Введiть кiнцеве значення Xmax: ");
            string sxMax = Console.ReadLine();
            double xMax = double.Parse(sxMax);
            Console.Write("Введiть прирiст dX: ");
            string sdx = Console.ReadLine();
            double dx = double.Parse(sdx);
            double x = xMin;
            double y;
            double svz = Math.Pow(x, 13) + 3 / Math.Cos(3 * x);
            double fullsum = x;

            while (x <= xMax)
            {
                y = Math.Sqrt(Math.Pow(Math.Cos(x), 3) + 3 * x) / svz;
                Console.WriteLine("x = {0}\t\t y = {1}", x, y);
                x += dx;
                fullsum += y;
            }
            if (Math.Abs(x - xMax - dx) > 0.0001)
            {
                y = Math.Pow(xMax, 2);
                Console.WriteLine("x = {0}\t\t y = {1}", xMax, y);
            }
            Console.WriteLine("Сумма промежуточных результатов :" + fullsum);
            Console.ReadKey();
        }
    }
}